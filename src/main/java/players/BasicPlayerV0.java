package players;

import de.upb.isml.thegamef2f.engine.CardPosition;
import de.upb.isml.thegamef2f.engine.GameState;
import de.upb.isml.thegamef2f.engine.Move;
import de.upb.isml.thegamef2f.engine.Placement;
import de.upb.isml.thegamef2f.engine.board.Card;
import de.upb.isml.thegamef2f.engine.player.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Basic Player AI
 * based on RandomPlayer from Alexander Tornede
 * @author fjsauer
 *
 * Functionality:
 *      Special Method hierarchy:
 *          IsBackwardsTrick()
 *          IsHelpEnemyWithoutConsequences()
 *          IsSelectMoveByOrder()
 *          Choose random placement, if nothing else applies.
 *      Other rules:
 *          None
 */

public class BasicPlayerV0 implements Player {

    private Random random;
    private GameState latestGameState;

    @Override
    public void initialize(long randomSeed) {
        this.random = new Random(randomSeed);
    }

    private Placement IsBackwardsTrick(GameState currentGameState, List<Placement> validPlacements)
    {
        /* checks if one of the valid placements is a BackwardsTrick placement.
        * If true, placement is returned
        * If false, null is returned */
        for (Placement placement : validPlacements)
        {
            if( (currentGameState.getTopCardOnOwnAscendingDiscardPile().getNumber() == placement.getCard().getNumber() + 10 && placement.getPosition() == CardPosition.OWN_ASCENDING_DISCARD_PILE) ||
                    (currentGameState.getTopCardOnOwnDescendingDiscardPile().getNumber() == placement.getCard().getNumber() - 10 && placement.getPosition() == CardPosition.OWN_DESCENDING_DISCARD_PILE) )
            {
                return placement;
            }
        }
        return null;
    }

    private Placement IsHelpEnemyWithoutConsequences(GameState currentGameState, List<Placement> validPlacements)
    {
        /* checks if one of the valid placements is a HelpEnemyWithoutConsequences placement.
         * If true, placement is returned
         * If false, null is returned */
        for (Placement placement : validPlacements)
        {
            if( (currentGameState.getTopCardOnOpponentsAscendingDiscardPile().getNumber() == placement.getCard().getNumber() + 1 && placement.getPosition() == CardPosition.OPPONENTS_ASCENDING_DISCARD_PILE) ||
                    (currentGameState.getTopCardOnOpponentsDescendingDiscardPile().getNumber() == placement.getCard().getNumber() - 1 && placement.getPosition() == CardPosition.OPPONENTS_DESCENDING_DISCARD_PILE) )
            {
                return placement;
            }
        }
        return null;
    }

    private Placement IsSelectMoveByOrder(GameState currentGameState, List<Placement> validPlacements)
    {
        Placement bestPlacementASC = null;
        Placement bestPlacementDES = null;
        for (Placement placement : validPlacements)
        {
            if (placement.getPosition() == CardPosition.OWN_ASCENDING_DISCARD_PILE)
            {
                if (bestPlacementASC == null)
                {
                    bestPlacementASC = placement;
                }
                if (bestPlacementASC.getCard().getNumber() > placement.getCard().getNumber())
                {
                    bestPlacementASC = placement;
                }
            }
            else if (placement.getPosition() == CardPosition.OWN_DESCENDING_DISCARD_PILE)
            {
                if (bestPlacementDES == null)
                {
                    bestPlacementDES = placement;
                }
                if (bestPlacementDES.getCard().getNumber() < placement.getCard().getNumber())
                {
                    bestPlacementDES = placement;
                }
            }
        }
        int rand = random.nextInt(2);
        if (rand == 0 && bestPlacementASC != null)
        {
            return bestPlacementASC;
        }
        else if (rand == 1 && bestPlacementDES != null)
        {
            return bestPlacementDES;
        }
        return null;
    }

    @Override
    public Move computeMove(GameState newGameState) {
        latestGameState = newGameState;

        boolean placedOnOpponentsPiles = false;

        List<Placement> placementsOfMove = new ArrayList<>();

        // as long as we still have hand cards
        while (!latestGameState.getHandCards().isEmpty()) {
            List<Placement> validPlacements = new ArrayList<>();

            // compute all valid placements
            for (Card card : latestGameState.getHandCards()) {
                for (CardPosition position : CardPosition.values()) {
                    Placement placement = new Placement(card, position);
                    if (isPlacementValid(placement, latestGameState, !placedOnOpponentsPiles)) {
                        validPlacements.add(placement);
                    }
                }
            }

            // if we cannot find a valid placement anymore, we can stop here and return the
            // ones we have so far
            if (validPlacements.isEmpty()) {
                return new Move(placementsOfMove);
            }

            //Pick next placement
            Placement nextPlacement = null;
            nextPlacement = IsBackwardsTrick(latestGameState, validPlacements); // perform backwards trick if available
            if (nextPlacement == null)
                nextPlacement = IsHelpEnemyWithoutConsequences(latestGameState, validPlacements); // perform help enemy if available
            if (nextPlacement == null)
                nextPlacement = IsSelectMoveByOrder(latestGameState, validPlacements); // perform card sorting to choose best possible card
            if (nextPlacement == null)
                nextPlacement = validPlacements.get(random.nextInt(validPlacements.size())); // pick random move if no special rule applied


            if (nextPlacement.getPosition() == CardPosition.OPPONENTS_ASCENDING_DISCARD_PILE
                    || nextPlacement.getPosition() == CardPosition.OPPONENTS_DESCENDING_DISCARD_PILE) {
                placedOnOpponentsPiles = true;
            }
            // add this random placement to the placements for our move
            placementsOfMove.add(nextPlacement);

            // update the view we have on the game to make sure that the next set of valid
            // placements is indeed valid
            latestGameState = computeNewGameStateAfterPlacement(latestGameState, nextPlacement);
        }
        return new Move(placementsOfMove);
    }

    private GameState computeNewGameStateAfterPlacement(GameState gameStatePriorToPlacement, Placement placement) {
        List<Card> handCards = new ArrayList<>(gameStatePriorToPlacement.getHandCards());
        handCards.remove(placement.getCard());

        List<Card> cardsOnOwnAscendingDiscardPile = new ArrayList<>(
                gameStatePriorToPlacement.getCardsOnOwnAscendingDiscardPile());
        if (placement.getPosition() == CardPosition.OWN_ASCENDING_DISCARD_PILE) {
            cardsOnOwnAscendingDiscardPile.add(placement.getCard());
        }

        List<Card> cardsOnOwnDescendingDiscardPile = new ArrayList<>(
                gameStatePriorToPlacement.getCardsOnOwnDescendingDiscardPile());
        if (placement.getPosition() == CardPosition.OWN_DESCENDING_DISCARD_PILE) {
            cardsOnOwnDescendingDiscardPile.add(placement.getCard());
        }

        List<Card> cardsOnOpponentsAscendingDiscardPile = new ArrayList<>(
                gameStatePriorToPlacement.getCardsOnOpponentsAscendingDiscardPile());
        if (placement.getPosition() == CardPosition.OPPONENTS_ASCENDING_DISCARD_PILE) {
            cardsOnOpponentsAscendingDiscardPile.add(placement.getCard());
        }

        List<Card> cardsOnOpponentsDescendingDiscardPile = new ArrayList<>(
                gameStatePriorToPlacement.getCardsOnOpponentsDescendingDiscardPile());
        if (placement.getPosition() == CardPosition.OPPONENTS_ASCENDING_DISCARD_PILE) {
            cardsOnOpponentsDescendingDiscardPile.add(placement.getCard());
        }

        return new GameState(handCards, cardsOnOwnAscendingDiscardPile, cardsOnOwnDescendingDiscardPile,
                cardsOnOpponentsAscendingDiscardPile, cardsOnOpponentsDescendingDiscardPile);
    }

    private boolean isPlacementValid(Placement placement, GameState gameState, boolean placingOnOpponentPilesAllowed) {
        switch (placement.getPosition()) {
            case OPPONENTS_ASCENDING_DISCARD_PILE:
                return placingOnOpponentPilesAllowed
                        ? canPlaceCardOnOpponentsAscendingDiscardPile(placement.getCard(), gameState)
                        : false;
            case OPPONENTS_DESCENDING_DISCARD_PILE:
                return placingOnOpponentPilesAllowed
                        ? canPlaceCardOnOpponentsDescendingDiscardPile(placement.getCard(), gameState)
                        : false;
            case OWN_ASCENDING_DISCARD_PILE:
                return canPlaceCardOnOwnAscendingDiscardPile(placement.getCard(), gameState);
            case OWN_DESCENDING_DISCARD_PILE:
                return canPlaceCardOnOwnDescendingDiscardPile(placement.getCard(), gameState);
        }
        return false;
    }

    private boolean canPlaceCardOnOwnAscendingDiscardPile(Card card, GameState gameState) {
        return gameState.getTopCardOnOwnAscendingDiscardPile().isSmallerThan(card)
                || gameState.getTopCardOnOwnAscendingDiscardPile().is10LargerThan(card);
    }

    private boolean canPlaceCardOnOwnDescendingDiscardPile(Card card, GameState gameState) {
        return card.isSmallerThan(gameState.getTopCardOnOwnDescendingDiscardPile())
                || card.is10LargerThan(gameState.getTopCardOnOwnDescendingDiscardPile());
    }

    private boolean canPlaceCardOnOpponentsAscendingDiscardPile(Card card, GameState gameState) {
        return card.isSmallerThan(gameState.getTopCardOnOpponentsAscendingDiscardPile());
    }

    private boolean canPlaceCardOnOpponentsDescendingDiscardPile(Card card, GameState gameState) {
        return gameState.getTopCardOnOpponentsDescendingDiscardPile().isSmallerThan(card);
    }

    @Override
    public String toString() {
        return getName();
    }

}