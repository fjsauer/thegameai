package players;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import de.upb.isml.thegamef2f.engine.CardPosition;
import de.upb.isml.thegamef2f.engine.GameState;
import de.upb.isml.thegamef2f.engine.Move;
import de.upb.isml.thegamef2f.engine.Placement;
import de.upb.isml.thegamef2f.engine.board.Card;
import de.upb.isml.thegamef2f.engine.player.Player;

/**
 * This class is an example implementation of an AI based on a random strategy.
 * At each turn, the AI constructs a move in the following iterative manner: As
 * long as the AI still have hand cards, it generates all valid placements using
 * its hand cards and uniformly at random chooses one of these placements. It
 * continues this process until it either has no more hand cards or cannot
 * generate a valid placement anymore. The resulting list of placements is
 * finally converted into a move.
 *
 * @author Alexander Tornede
 *
 * Functionality:
 *      Special Method hierarchy:
 *          Choose random placement.
 *      Other rules:
 *          None
 */



public class RandomPlayer implements Player {

	private Random random;

	@Override
	public void initialize(long randomSeed) {
		this.random = new Random(randomSeed);
	}

	@Override
	public Move computeMove(GameState gameState) {
		GameState currentGameState = gameState;
		boolean placedOnOpponentsPiles = false;

		List<Placement> placementsOfMove = new ArrayList<>();

		// as long as we still have hand cards
		while (!currentGameState.getHandCards().isEmpty()) {
			List<Placement> validPlacements = new ArrayList<>();

			// compute all valid placements
			for (Card card : currentGameState.getHandCards()) {
				for (CardPosition position : CardPosition.values()) {
					Placement placement = new Placement(card, position);
					if (isPlacementValid(placement, currentGameState, !placedOnOpponentsPiles)) {
						validPlacements.add(placement);
					}
				}
			}

			// if we cannot find a valid placement anymore, we can stop here and return the
			// ones we have so far
			if (validPlacements.isEmpty()) {
				return new Move(placementsOfMove);
			}

			// pick a random placement out of the valid ones
			Placement randomPlacement = validPlacements.get(random.nextInt(validPlacements.size()));
			if (randomPlacement.getPosition() == CardPosition.OPPONENTS_ASCENDING_DISCARD_PILE
					|| randomPlacement.getPosition() == CardPosition.OPPONENTS_DESCENDING_DISCARD_PILE) {
				placedOnOpponentsPiles = true;
			}
			// add this random placement to the placements for our move
			placementsOfMove.add(randomPlacement);

			// update the view we have on the game to make sure that the next set of valid
			// placements is indeed valid
			currentGameState = computeNewGameStateAfterPlacement(currentGameState, randomPlacement);
		}

		return new Move(placementsOfMove);
	}

	private GameState computeNewGameStateAfterPlacement(GameState gameStatePriorToPlacement, Placement placement) {
		List<Card> handCards = new ArrayList<>(gameStatePriorToPlacement.getHandCards());
		handCards.remove(placement.getCard());

		List<Card> cardsOnOwnAscendingDiscardPile = new ArrayList<>(
				gameStatePriorToPlacement.getCardsOnOwnAscendingDiscardPile());
		if (placement.getPosition() == CardPosition.OWN_ASCENDING_DISCARD_PILE) {
			cardsOnOwnAscendingDiscardPile.add(placement.getCard());
		}

		List<Card> cardsOnOwnDescendingDiscardPile = new ArrayList<>(
				gameStatePriorToPlacement.getCardsOnOwnDescendingDiscardPile());
		if (placement.getPosition() == CardPosition.OWN_DESCENDING_DISCARD_PILE) {
			cardsOnOwnDescendingDiscardPile.add(placement.getCard());
		}

		List<Card> cardsOnOpponentsAscendingDiscardPile = new ArrayList<>(
				gameStatePriorToPlacement.getCardsOnOpponentsAscendingDiscardPile());
		if (placement.getPosition() == CardPosition.OPPONENTS_ASCENDING_DISCARD_PILE) {
			cardsOnOpponentsAscendingDiscardPile.add(placement.getCard());
		}

		List<Card> cardsOnOpponentsDescendingDiscardPile = new ArrayList<>(
				gameStatePriorToPlacement.getCardsOnOpponentsDescendingDiscardPile());
		if (placement.getPosition() == CardPosition.OPPONENTS_DESCENDING_DISCARD_PILE) {
			cardsOnOpponentsDescendingDiscardPile.add(placement.getCard());
		}

		return new GameState(handCards, cardsOnOwnAscendingDiscardPile, cardsOnOwnDescendingDiscardPile,
				cardsOnOpponentsAscendingDiscardPile, cardsOnOpponentsDescendingDiscardPile);
	}

	private boolean isPlacementValid(Placement placement, GameState gameState, boolean placingOnOpponentPilesAllowed) {
		switch (placement.getPosition()) {
		case OPPONENTS_ASCENDING_DISCARD_PILE:
			return placingOnOpponentPilesAllowed
					? canPlaceCardOnOpponentsAscendingDiscardPile(placement.getCard(), gameState)
					: false;
		case OPPONENTS_DESCENDING_DISCARD_PILE:
			return placingOnOpponentPilesAllowed
					? canPlaceCardOnOpponentsDescendingDiscardPile(placement.getCard(), gameState)
					: false;
		case OWN_ASCENDING_DISCARD_PILE:
			return canPlaceCardOnOwnAscendingDiscardPile(placement.getCard(), gameState);
		case OWN_DESCENDING_DISCARD_PILE:
			return canPlaceCardOnOwnDescendingDiscardPile(placement.getCard(), gameState);
		}
		return false;
	}

	private boolean canPlaceCardOnOwnAscendingDiscardPile(Card card, GameState gameState) {
		return gameState.getTopCardOnOwnAscendingDiscardPile().isSmallerThan(card)
				|| gameState.getTopCardOnOwnAscendingDiscardPile().is10LargerThan(card);
	}

	private boolean canPlaceCardOnOwnDescendingDiscardPile(Card card, GameState gameState) {
		return card.isSmallerThan(gameState.getTopCardOnOwnDescendingDiscardPile())
				|| card.is10LargerThan(gameState.getTopCardOnOwnDescendingDiscardPile());
	}

	private boolean canPlaceCardOnOpponentsAscendingDiscardPile(Card card, GameState gameState) {
		return card.isSmallerThan(gameState.getTopCardOnOpponentsAscendingDiscardPile());
	}

	private boolean canPlaceCardOnOpponentsDescendingDiscardPile(Card card, GameState gameState) {
		return gameState.getTopCardOnOpponentsDescendingDiscardPile().isSmallerThan(card);
	}

	@Override
	public String toString() {
		return getName();
	}

}