package players;

import de.upb.isml.thegamef2f.engine.CardPosition;
import de.upb.isml.thegamef2f.engine.GameState;
import de.upb.isml.thegamef2f.engine.Move;
import de.upb.isml.thegamef2f.engine.Placement;
import de.upb.isml.thegamef2f.engine.board.Card;
import de.upb.isml.thegamef2f.engine.player.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

/**
 * Real Player Interface
 * based on RandomPlayer from Alexander Tornede
 * @author fjsauer
 *
 * Functionality:
 *      gives real player the ability to choose move from random moves.
 */



public class RealPlayer implements Player {

	private Random random;
	Scanner sc = new Scanner(System.in);

	@Override
	public void initialize(long randomSeed) {
		this.random = new Random(randomSeed);
	}

	private Placement ChoosePlacement(GameState currentGameState, List<Placement> validPlacements)
	{
		while (true)
		{
			System.out.println("_________ Enemy: ________");
			System.out.println("ASC [ID:1]: "+ currentGameState.getTopCardOnOpponentsAscendingDiscardPile().getNumber() + " (Number of cards: "+ currentGameState.getCardsOnOpponentsAscendingDiscardPile().size()+")");
			System.out.println("DES [ID:2]: "+ currentGameState.getTopCardOnOpponentsDescendingDiscardPile().getNumber() + " (Number of cards: "+ currentGameState.getCardsOnOpponentsDescendingDiscardPile().size()+")");
			System.out.println("_________ You: __________");
			System.out.println("DES [ID:3]: "+ currentGameState.getTopCardOnOwnDescendingDiscardPile().getNumber() + " (Number of cards: "+ currentGameState.getCardsOnOwnDescendingDiscardPile().size()+")");
			System.out.println("ASC [ID:4]: "+ currentGameState.getTopCardOnOwnAscendingDiscardPile().getNumber() + " (Number of cards: "+ currentGameState.getCardsOnOwnAscendingDiscardPile().size()+")");
			System.out.println("Hand cards: " + currentGameState.getHandCards());
			System.out.println("___ Choose placement: ___");


			int cardNumber = -1;
			do {
				System.out.println("Card (0 for 'No more moves'): ");
				while (!sc.hasNextInt()) {
					System.out.println("That's not an integer. Try again.");
					sc.next();
				}
				cardNumber = sc.nextInt();
			} while (cardNumber <= -1);

			if (cardNumber == 0)
			{
				return null;
			}

			int pileID = -1;
			do {
				System.out.println("Discard pile ID: ");
				while (!sc.hasNextInt()) {
					System.out.println("That's not an integer. Try again.");
					sc.next();
				}
				pileID = sc.nextInt();
			} while (pileID <= -1);

			CardPosition chosenCardPosition = null;
			if (pileID == 1)
			{
				chosenCardPosition = CardPosition.OPPONENTS_ASCENDING_DISCARD_PILE;
			}
			else if (pileID == 2)
			{
				chosenCardPosition = CardPosition.OPPONENTS_DESCENDING_DISCARD_PILE;
			}
			else if (pileID == 3)
			{
				chosenCardPosition = CardPosition.OWN_DESCENDING_DISCARD_PILE;
			}
			else if (pileID == 4)
			{
				chosenCardPosition = CardPosition.OWN_ASCENDING_DISCARD_PILE;
			}
			else
			{
				System.out.println("Invalid CardPosition ID specified. Valid options are 0, 1, 2, 3.");
			}

			for (Placement placement : validPlacements)
			{
				if (placement.getCard().getNumber() == cardNumber && placement.getPosition() == chosenCardPosition)
				{
					return placement;
				}
			}
			System.out.println("No valid placement matches specified card number and Pile ID. Try again.");
		}
	}

	@Override
	public Move computeMove(GameState gameState) {
		GameState currentGameState = gameState;
		boolean placedOnOpponentsPiles = false;

		List<Placement> placementsOfMove = new ArrayList<>();

		// as long as we still have hand cards
		while (!currentGameState.getHandCards().isEmpty()) {
			List<Placement> validPlacements = new ArrayList<>();

			// compute all valid placements
			for (Card card : currentGameState.getHandCards()) {
				for (CardPosition position : CardPosition.values()) {
					Placement placement = new Placement(card, position);
					if (isPlacementValid(placement, currentGameState, !placedOnOpponentsPiles)) {
						validPlacements.add(placement);
					}
				}
			}

			// if we cannot find a valid placement anymore, we can stop here and return the
			// ones we have so far
			if (validPlacements.isEmpty()) {
				return new Move(placementsOfMove);
			}

			// pick a random placement out of the valid ones
			Placement chosenPlacement = ChoosePlacement(currentGameState, validPlacements);

			// break from while loop if no further placement is chosen
			if (chosenPlacement == null)
			{
				break;
			}

			if (chosenPlacement.getPosition() == CardPosition.OPPONENTS_ASCENDING_DISCARD_PILE
					|| chosenPlacement.getPosition() == CardPosition.OPPONENTS_DESCENDING_DISCARD_PILE) {
				placedOnOpponentsPiles = true;
			}


			// add this random placement to the placements for our move
			placementsOfMove.add(chosenPlacement);

			// update the view we have on the game to make sure that the next set of valid
			// placements is indeed valid
			currentGameState = computeNewGameStateAfterPlacement(currentGameState, chosenPlacement);
		}

		return new Move(placementsOfMove);
	}

	private GameState computeNewGameStateAfterPlacement(GameState gameStatePriorToPlacement, Placement placement) {
		List<Card> handCards = new ArrayList<>(gameStatePriorToPlacement.getHandCards());
		handCards.remove(placement.getCard());

		List<Card> cardsOnOwnAscendingDiscardPile = new ArrayList<>(
				gameStatePriorToPlacement.getCardsOnOwnAscendingDiscardPile());
		if (placement.getPosition() == CardPosition.OWN_ASCENDING_DISCARD_PILE) {
			cardsOnOwnAscendingDiscardPile.add(placement.getCard());
		}

		List<Card> cardsOnOwnDescendingDiscardPile = new ArrayList<>(
				gameStatePriorToPlacement.getCardsOnOwnDescendingDiscardPile());
		if (placement.getPosition() == CardPosition.OWN_DESCENDING_DISCARD_PILE) {
			cardsOnOwnDescendingDiscardPile.add(placement.getCard());
		}

		List<Card> cardsOnOpponentsAscendingDiscardPile = new ArrayList<>(
				gameStatePriorToPlacement.getCardsOnOpponentsAscendingDiscardPile());
		if (placement.getPosition() == CardPosition.OPPONENTS_ASCENDING_DISCARD_PILE) {
			cardsOnOpponentsAscendingDiscardPile.add(placement.getCard());
		}

		List<Card> cardsOnOpponentsDescendingDiscardPile = new ArrayList<>(
				gameStatePriorToPlacement.getCardsOnOpponentsDescendingDiscardPile());
		if (placement.getPosition() == CardPosition.OPPONENTS_ASCENDING_DISCARD_PILE) {
			cardsOnOpponentsDescendingDiscardPile.add(placement.getCard());
		}

		return new GameState(handCards, cardsOnOwnAscendingDiscardPile, cardsOnOwnDescendingDiscardPile,
				cardsOnOpponentsAscendingDiscardPile, cardsOnOpponentsDescendingDiscardPile);
	}

	private boolean isPlacementValid(Placement placement, GameState gameState, boolean placingOnOpponentPilesAllowed) {
		switch (placement.getPosition()) {
		case OPPONENTS_ASCENDING_DISCARD_PILE:
			return placingOnOpponentPilesAllowed
					? canPlaceCardOnOpponentsAscendingDiscardPile(placement.getCard(), gameState)
					: false;
		case OPPONENTS_DESCENDING_DISCARD_PILE:
			return placingOnOpponentPilesAllowed
					? canPlaceCardOnOpponentsDescendingDiscardPile(placement.getCard(), gameState)
					: false;
		case OWN_ASCENDING_DISCARD_PILE:
			return canPlaceCardOnOwnAscendingDiscardPile(placement.getCard(), gameState);
		case OWN_DESCENDING_DISCARD_PILE:
			return canPlaceCardOnOwnDescendingDiscardPile(placement.getCard(), gameState);
		}
		return false;
	}

	private boolean canPlaceCardOnOwnAscendingDiscardPile(Card card, GameState gameState) {
		return gameState.getTopCardOnOwnAscendingDiscardPile().isSmallerThan(card)
				|| gameState.getTopCardOnOwnAscendingDiscardPile().is10LargerThan(card);
	}

	private boolean canPlaceCardOnOwnDescendingDiscardPile(Card card, GameState gameState) {
		return card.isSmallerThan(gameState.getTopCardOnOwnDescendingDiscardPile())
				|| card.is10LargerThan(gameState.getTopCardOnOwnDescendingDiscardPile());
	}

	private boolean canPlaceCardOnOpponentsAscendingDiscardPile(Card card, GameState gameState) {
		return card.isSmallerThan(gameState.getTopCardOnOpponentsAscendingDiscardPile());
	}

	private boolean canPlaceCardOnOpponentsDescendingDiscardPile(Card card, GameState gameState) {
		return gameState.getTopCardOnOpponentsDescendingDiscardPile().isSmallerThan(card);
	}

	@Override
	public String toString() {
		return getName();
	}

}