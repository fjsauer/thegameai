import de.upb.isml.thegamef2f.engine.GameHistory;
import de.upb.isml.thegamef2f.engine.board.Game;
import de.upb.isml.thegamef2f.engine.player.Player;
import players.*;
import util.Extensions.LoggingSystem;
import util.Extensions.Statistics;

import java.io.IOException;
import java.util.Random;

public class MainClass {
    public static void main(String[] args) throws IOException {
        //OptimizeWeights optimizer = new OptimizeWeights();
        //optimizer.Compute();
        RunSimulation();
    }

    public static void RunSimulation()
    {
        // USER INPUT
        int numGames =  10000;

        Player p1 = new RandomPlayer();
        Player p2 = new OptimizeTurnPlayer();

        Statistics stats = new Statistics(p1, p2);
        Random rand = new Random();

        for (int i = 0; i < numGames; i = i + 1) // run specified number of games
        {
            if (i % 1000 == 0)
            {
                System.out.println("Games played: " + i);
            }
            Game game;
            if (i % 2 == 0) // switch starting player after every game
            {
                if (numGames == 1)
                {
                    game = new Game(p1, p2, 69420);
                }
                else
                {
                    game = new Game(p1, p2, rand.nextLong());
                }
            }
            else
            {
                if (numGames == 1)
                {
                    game = new Game(p2, p1, 69420);
                }
                else
                {
                    game = new Game(p2, p1, rand.nextLong());
                }
            }

            Player winner = game.simulate(); // run games simulation
            //game.getHistory().printHistory();
            stats.TrackGame(winner, game.getHistory());
            //System.out.println(p1.getName() + ": " + ((OptimizeTurnPlayerDefault)p1).GetCntTurns() + " | " +p2.getName() + ": " +((OptimizeTurnPlayer)p2).GetCntTurns());
        }

        stats.PrintStats();
    }
}
