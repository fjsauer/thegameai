package util.Extensions;

import de.upb.isml.thegamef2f.engine.Placement;

public class RatedPlacement {
    public Placement placement;
    public float resultingGameValue;

    public RatedPlacement(Placement placement, float gameValue)
    {
        this.placement = placement;
        this.resultingGameValue = gameValue;
    }
}
