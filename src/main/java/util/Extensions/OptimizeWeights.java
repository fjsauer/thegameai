package util.Extensions;

import de.upb.isml.thegamef2f.engine.board.Game;
import de.upb.isml.thegamef2f.engine.player.Player;
import players.BasicPlayer;
import players.OptimizeTurnPlayer;
import players.OptimizeTurnPlayerDefault;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class OptimizeWeights {
    private int sizeThreshold = 60;
    private int startThreshold = 0;
    private int stepThreshold = 1;

    private int sizeWeightOwn = 1;
    private float startWeightOwn = 1.0f;
    private float stepWeightOwn = 1.0f;

    private int sizeWeightOpp = 1;
    private float startWeightOpp = 0.2f;
    private float stepWeightOpp = 0.02f;


    private List<Integer> listThreshold = new ArrayList<Integer>();
    private List<Float> listWeightOwn = new ArrayList<Float>();
    private List<Float> listWeightOpp = new ArrayList<Float>();

    private List<Result> results = new ArrayList<Result>();

    public OptimizeWeights()
    {

        for (int i = 0; i < sizeThreshold; i++)
        {
            listThreshold.add(startThreshold + stepThreshold * i);
        }

        for (int i = 0; i < sizeWeightOwn; i++)
        {
            listWeightOwn.add(startWeightOwn + stepWeightOwn * i);
        }

        for (int i = 0; i < sizeWeightOpp; i++)
        {
            listWeightOpp.add(startWeightOpp + stepWeightOpp * i);
        }
    }

    public void Compute() throws IOException
    {
        PrintWriter pw = new PrintWriter ("results.txt");
        pw.write("WinRate,\tThreshold,\tWeightOwn,\tWeightOpp\n");
        System.out.println("WinRate,\tThreshold,\tWeightOwn,\tWeightOpp");

        for (int badPlacementThreshold : listThreshold)
        {
            for (float weightOwn : listWeightOwn)
            {
                for (float weightOpp : listWeightOpp)
                {
                    Result result = Simulate(badPlacementThreshold, weightOwn, weightOpp);
                    result.Print(pw);
                    results.add(result);
                }
            }
        }
        pw.close();
    }

    private Result Simulate(int badPlacementThreshold, float weightOwn, float weightOpp)
    {
        // USER INPUT
        int numGames =  2500;

        Player p1 = new OptimizeTurnPlayerDefault( false);
        Player p2 = new OptimizeTurnPlayer(badPlacementThreshold, weightOwn, weightOpp, false);

        Statistics stats = new Statistics(p1, p2);
        Random rand = new Random();

        for (int i = 0; i < numGames; i = i + 1) // run specified number of games
        {
            Game game;
            if (i % 2 == 0) // switch starting player after every game
            {
                game = new Game(p1, p2, rand.nextLong());
            }
            else
            {
                game = new Game(p2, p1, rand.nextLong());
            }

            Player winner = game.simulate(); // run games simulation
            //game.getHistory().printHistory();
            stats.TrackGame(winner, game.getHistory());
        }

        return new Result(stats.GetWinRateP2(), badPlacementThreshold, weightOwn, weightOpp);
    }
}
