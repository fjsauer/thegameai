package util.Extensions;

import de.upb.isml.thegamef2f.engine.GameState;

public class GameValue {

        private LoggingSystem logger;
        private StackValue ownAsc;
        private StackValue ownDes;
        private StackValue oppAsc;
        private StackValue oppDes;

        private float gameValue = 0.0f;

        private float weightOwn = 1.0f;
        private float weightOpp = 0.2f;

        public void SetWeights(float weightOwn, float weightOpp)
        {
                this.weightOwn = weightOwn;
                this.weightOpp = weightOpp;
        }

        public GameValue(LoggingSystem logger)
        {
                this.logger = logger;
                ownAsc = new StackValue(true, true, logger);
                ownDes = new StackValue(true, false, logger);
                oppAsc = new StackValue(false, true, logger);
                oppDes = new StackValue(false, false, logger);
        }

        public float calcGameValue(GameState potentialGameState, CardTracker cardTracker)
        {
                logger.Log("\nHand cards: " + potentialGameState.getHandCards());
                logger.Log("OwnAsc: " + potentialGameState.getTopCardOnOwnAscendingDiscardPile());
                logger.Log("OwnDes: " + potentialGameState.getTopCardOnOwnDescendingDiscardPile());
                logger.Log("OppAsc: " + potentialGameState.getTopCardOnOpponentsAscendingDiscardPile());
                logger.Log("OppDes: " + potentialGameState.getTopCardOnOpponentsDescendingDiscardPile() + "\n");
                ownAsc.calcValues(potentialGameState, cardTracker, weightOwn, weightOpp);
                ownDes.calcValues(potentialGameState, cardTracker, weightOwn, weightOpp);
                oppAsc.calcValues(potentialGameState, cardTracker, weightOwn, weightOpp);
                oppDes.calcValues(potentialGameState, cardTracker, weightOwn, weightOpp);
                gameValue = ownAsc.GetValue() + ownDes.GetValue() - oppAsc.GetValue() - oppDes.GetValue();

                logger.Log("GameValue: " + gameValue);
                logger.Log("___________________________________________________");

                return gameValue;
        }

        public void setWeightOwn(float weight)
        {
                weightOwn = weight;
        }

        public void setWeightOpp(float weight)
        {
                weightOpp = weight;
        }
}
