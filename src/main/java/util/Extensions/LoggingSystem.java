package util.Extensions;

public class LoggingSystem {
    boolean isDebug = true;

    public LoggingSystem(boolean mode)
    {
        isDebug = mode;
    }

    public void SetDebugMode(boolean mode)
    {
        isDebug = mode;
    }


    public void Log(String message)
    {
        if(isDebug)
        {
            System.out.println(message);
        }
    }
}
