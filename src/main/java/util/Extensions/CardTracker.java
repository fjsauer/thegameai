package util.Extensions;

import de.upb.isml.thegamef2f.engine.Placement;
import de.upb.isml.thegamef2f.engine.board.Card;

import java.util.ArrayList;
import java.util.List;

public class CardTracker {
    public List<Card> cardsPlacedByMe = new ArrayList<Card>();
    public List<Card> cardsPlacedByOp = new ArrayList<Card>();

    public CardTracker()
    {

    }

    public CardTracker(CardTracker cardTracker)
    {
        this.cardsPlacedByMe = new ArrayList<Card>(cardTracker.cardsPlacedByMe);
        this.cardsPlacedByOp = new ArrayList<Card>(cardTracker.cardsPlacedByOp);
    }

    public void TrackPlacedCardsByMe(List<Placement> placementsOfMove)
    {
        for (Placement placement : placementsOfMove)
        {
            cardsPlacedByMe.add(placement.getCard());
        }
        int i = 3;
    }

    public void TrackPlacedCardsByOp(List<ExtendedPlacement> placementsOfMove)
    {
        for (ExtendedPlacement placement : placementsOfMove)
        {
            cardsPlacedByOp.add(placement.getCard());
        }
        int i = 3;
    }

    public void PrintTrackedCards()
    {
        System.out.println("Cards played by me: " + cardsPlacedByMe);
        System.out.println("Cards played by op: " + cardsPlacedByOp);
    }
}
