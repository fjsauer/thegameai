package util.Extensions;

import de.upb.isml.thegamef2f.engine.CardPosition;
import de.upb.isml.thegamef2f.engine.Placement;
import de.upb.isml.thegamef2f.engine.board.Card;

public class ExtendedPlacement extends Placement{

    private Card prevTopCard;

    public ExtendedPlacement(Card card, CardPosition position, Card prevTopCard) {
        super(card, position);
        this.prevTopCard = prevTopCard;
    }

    public Card GetPrevTopCard()
    {
        return prevTopCard;
    }
}
