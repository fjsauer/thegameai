package util.Extensions;

import de.upb.isml.thegamef2f.engine.GameHistory;
import de.upb.isml.thegamef2f.engine.player.Player;

import static java.lang.String.format;

public class Statistics {
    private Player p1;
    private Player p2;

    private int numGames = 0;
    private int p1Wins = 0;
    private int p2Wins = 0;

    public Statistics(Player p1, Player p2)
    {
        this.p1 = p1;
        this.p2 = p2;
    }

    public void TrackGame(Player winner, GameHistory hist)
    {
        numGames += 1;
        if(winner.getName().equals(p1.getName()))
        {
            p1Wins += 1;
        }
        else if(winner.getName().equals(p2.getName()))
        {
            p2Wins += 1;
        }
        else
        {
            System.out.println("[Error]: Correct winner could not be determined.");
        }
    }

    public void PrintStats()
    {
        System.out.println(p1.getName() + " Wins: " + p1Wins + " (" + format("%.2f", (float)p1Wins / numGames * 100.0) + " %)");
        System.out.println(p2.getName() + " Wins: " + p2Wins + " (" + format("%.2f", (float)p2Wins / numGames * 100.0) + " %)");
    }

    public float GetWinRateP2()
    {
        return (float)p2Wins / numGames;
    }
}
