package util.Extensions;

import java.io.PrintWriter;

public class Result {
    private float winRate;
    private int badPlacementThreshold;
    private float weightOwn;
    private float weightOpp;

    public Result(float winRate, int badPlacementThreshold, float weightOwn, float weightOpp)
    {
        this.winRate = winRate;
        this.badPlacementThreshold = badPlacementThreshold;
        this.weightOwn = weightOwn;
        this.weightOpp = weightOpp;
    }

    public void Print(PrintWriter pw)
    {
        pw.write(winRate + ",\t" + badPlacementThreshold + ",\t" + weightOwn + ",\t" + weightOpp + "\n");
        System.out.println(winRate + ",\t" + badPlacementThreshold + ",\t" + weightOwn + ",\t" + weightOpp);
    }
}
