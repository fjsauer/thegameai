package util.Extensions;

import de.upb.isml.thegamef2f.engine.GameState;
import de.upb.isml.thegamef2f.engine.board.Card;

import java.util.List;

public class StackValue {
    LoggingSystem logger;

    public float value;

    private boolean isMine;
    private boolean isAscending;

    public int ownTheo;
    public int ownPrac;
    public int oppTheo;
    public int oppPrac;

    public StackValue(boolean isMine, boolean isAscending, LoggingSystem logger) {
        this.isMine = isMine;
        this.isAscending = isAscending;
        this.logger = logger;
    }

    public float GetValue() {
        return value;
    }

    private int GetNotSkippedCards(boolean isSmallerThanTopCard, List<Card> playedCards, Card topCard)
    {
        int notSkippedCards = 0;
        for (Card card : playedCards) {
            if (isSmallerThanTopCard)
            {
                if (card.getNumber() < topCard.getNumber()) {
                    notSkippedCards++;
                }
            }
            else
            {
                if (card.getNumber() > topCard.getNumber()) {
                    notSkippedCards++;
                }
            }
        }
        return notSkippedCards;
    }

    public void calcValues(GameState potentialGameState, CardTracker cardTracker, float weightOwn, float weightOpp) {
        Card topCard;
        int possibleCards;
        int alreadyPlayedCards;

        if (isMine)
        {
            ownTheo = 58 - cardTracker.cardsPlacedByMe.size();
            oppTheo = 58 - cardTracker.cardsPlacedByOp.size();

            if (isAscending)
            {
                topCard = potentialGameState.getTopCardOnOwnAscendingDiscardPile();

                if (topCard.getNumber() == 1)
                {
                    ownPrac = ownTheo;
                    oppPrac = 0;
                }
                else
                {
                    possibleCards = 59 - topCard.getNumber();
                    alreadyPlayedCards = GetNotSkippedCards(false, cardTracker.cardsPlacedByMe, topCard); //Alles, was ICH gespielt habe und > topCard.getNumber();
                    ownPrac = possibleCards - alreadyPlayedCards;

                    possibleCards = topCard.getNumber() - 2;
                    alreadyPlayedCards = GetNotSkippedCards(true, cardTracker.cardsPlacedByOp, topCard); //Alles, was ER gespielt hat und < topCard.GetNumber();
                    oppPrac = possibleCards - alreadyPlayedCards;
                }
            }
            else // isDescending
            {
                topCard = potentialGameState.getTopCardOnOwnDescendingDiscardPile();

                if (topCard.getNumber() == 60)
                {
                    ownPrac = ownTheo;
                    oppPrac = 0;
                }
                else
                {
                    possibleCards = topCard.getNumber() - 2;
                    alreadyPlayedCards = GetNotSkippedCards(true, cardTracker.cardsPlacedByMe, topCard); //Alles, was ICH gespielt habe und < topCard.GetNumber();
                    ownPrac = possibleCards - alreadyPlayedCards;

                    possibleCards = 59 - topCard.getNumber();
                    alreadyPlayedCards = GetNotSkippedCards(false, cardTracker.cardsPlacedByOp, topCard); //Alles, was ER gespielt hat und > topCard.GetNumber();
                    oppPrac = possibleCards - alreadyPlayedCards;
                }
            }
        }
        else // isOpponent
        {
            ownTheo = 58 - cardTracker.cardsPlacedByOp.size();
            oppTheo = 58 - cardTracker.cardsPlacedByMe.size();

            if (isAscending)
            {
                topCard = potentialGameState.getTopCardOnOpponentsAscendingDiscardPile();

                if (topCard.getNumber() == 1)
                {
                    ownPrac = ownTheo;
                    oppPrac = 0;
                }
                else
                {
                    possibleCards = 59 - topCard.getNumber();
                    alreadyPlayedCards = GetNotSkippedCards(false, cardTracker.cardsPlacedByOp, topCard); //Alles, was ER gespielt hat und > topCard.getNumber();
                    ownPrac = possibleCards - alreadyPlayedCards;

                    possibleCards = topCard.getNumber() - 2;
                    alreadyPlayedCards = GetNotSkippedCards(true, cardTracker.cardsPlacedByMe, topCard); //Alles, was ICH gespielt habe und < topCard.GetNumber();
                    oppPrac = possibleCards - alreadyPlayedCards;
                }
            }
            else // isDescending
            {
                topCard = potentialGameState.getTopCardOnOpponentsDescendingDiscardPile();

                if (topCard.getNumber() == 60)
                {
                    ownPrac = ownTheo;
                    oppPrac = 0;
                }
                else
                {
                    possibleCards = topCard.getNumber() - 2;
                    alreadyPlayedCards = GetNotSkippedCards(true, cardTracker.cardsPlacedByOp, topCard); //Alles, was ER gespielt habe und < topCard.GetNumber();
                    ownPrac = possibleCards - alreadyPlayedCards;

                    possibleCards = 59 - topCard.getNumber();
                    alreadyPlayedCards = GetNotSkippedCards(false, cardTracker.cardsPlacedByMe, topCard); //Alles, was ICH gespielt hat und > topCard.GetNumber();
                    oppPrac = possibleCards - alreadyPlayedCards;
                }
            }
        }

        value = weightOwn * (ownTheo - ownPrac) + weightOpp * (oppPrac - oppTheo);
        logger.Log("IsMine: " + isMine + " | IsAscending: " + isAscending);
        logger.Log("Stack value calc: " + weightOwn + " * (" + ownTheo + "-" + ownPrac + ") + " + weightOpp + " * ( " + oppPrac + " - " + oppTheo + " ) " + " = " + value);
    }
}
